package com.flexi.exception;

import java.io.IOException;

public class RetrofitNoConnectivityException extends IOException {

    public RetrofitNoConnectivityException() {
        this("No Internet Connectivity. Please check your Wifi or Data connection.");
    }

    public RetrofitNoConnectivityException(String message) {
        super(message);
    }
}
