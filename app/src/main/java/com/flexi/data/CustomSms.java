package com.flexi.data;

public class CustomSms {

    private long id;
    private long date;
    private int read;
    private String address;
    private String body;

    public CustomSms() {
    }

    public CustomSms(long id, long date, int read, String address, String body) {
        this.id = id;
        this.date = date;
        this.read = read;
        this.address = address;
        this.body = body;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public int getRead() {
        return read;
    }

    public void setRead(int read) {
        this.read = read;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
