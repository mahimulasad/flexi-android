package com.flexi.constant;

public class BundleKeys {

    public static final String COMMAND = "COMMAND";

    public static class Commands {
        public static final String STOP_SERVICE = "STOP_SERVICE";
        public static final String REFRESH = "REFRESH";
    }
}
