package com.flexi.service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.Nullable;

import com.flexi.R;
import com.flexi.constant.BundleKeys;
import com.flexi.manager.UssdManager;
import com.flexi.retrofit.impl.NewFelxiServiceImpl;

public class MainService extends Service {

    private static final String TAG = MainService.class.getSimpleName();

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate()");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String CHANNEL_ID = "channel_flexi";
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "Flexi Channel",
                    NotificationManager.IMPORTANCE_DEFAULT);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            if (manager != null) {
                manager.createNotificationChannel(channel);
            }

            Notification.Builder builder = new Notification.Builder(this, CHANNEL_ID)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Running")
                    .setAutoCancel(true);

            Notification notification = builder.build();
            startForeground(1, notification);
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand()");
        if (intent != null) {
            if (TextUtils.equals(intent.getAction(), BundleKeys.Commands.STOP_SERVICE)) {
                stopForeground(true);
                stopSelf();

                return START_NOT_STICKY;
            }
        }

        call();

        //Make it stick to the notification panel so it is less prone to get cancelled by the Operating System.
        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    private void call() {
        UssdManager.serial = 1;
        //UssdManager.sendAllUnreadSms(this);
        (new NewFelxiServiceImpl()).requestNewFlexi(this);
    }

}
