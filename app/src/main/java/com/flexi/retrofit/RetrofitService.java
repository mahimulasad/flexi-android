package com.flexi.retrofit;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface RetrofitService {

    @GET("androidmodem/newFlexi")
    Call<String> newFlexi(@Query("op") String op);

    @GET("androidmodem/insertSMS")
    Call<String> insertSms(@Query("sender") String sender, @Query("sms") String sms);

}
