package com.flexi.retrofit.impl;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.flexi.manager.RetrofitManager;
import com.flexi.manager.SharedPreferenceManager;
import com.flexi.manager.UssdManager;
import com.flexi.retrofit.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

import static com.flexi.manager.UssdManager.decideNextStep;
import static com.flexi.manager.UssdManager.serial;

public class NewFelxiServiceImpl {

    private Context context;

    public void requestNewFlexi(Context context) {
        this.context = context;
        Retrofit retrofit = RetrofitManager.newInstance(context);
        RetrofitService service = retrofit.create(RetrofitService.class);
        String op = (serial % 2 == 1) ? "gp" : "airtel";
        Call<String> call = service.newFlexi(op);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                Log.d("NewFlexiServiceImpl", "response: " + response.body());
                if (response.code() == 200) {
                    handleNewFlexiResponse(response.body());
                } else {
                    //requestNewFlexi(context);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //requestNewFlexi(context);
            }
        });
    }

    private void handleNewFlexiResponse(String response) {
        try {
            if (TextUtils.isEmpty(response) || TextUtils.isEmpty(response.trim())) {
                decideNextStep(context);
            } else {
                String[] token = response.split(";");
                String id = token[0];
                String number = token[1];
                String amount = token[2];
                String type = token[3];
                String opname = token[4];
//                String pin = ((serial % 3) == 1)
//                        ? SharedPreferenceManager.getSim1Pin(context)
//                        : SharedPreferenceManager.getSim2Pin(context);
                // Log.d("FlexiService", "op: "+ opname);
                String ussd = "";
                int simSlot = 0;
                if (opname.equals("gp")) {
                    ussd = "*222*" + number + "*" + amount + "*" + "0" + "*" + SharedPreferenceManager.getSim1Pin(context) + "#";
                    simSlot = 0;
                } else if (opname.equals("airtel")) {
                    ussd = "*999*" + number + "*" + amount + "*" + SharedPreferenceManager.getSim2Pin(context) + "#";
                    simSlot = 1;
                }
                Log.d("NewFlexiServiceImpl", "ussd: " + ussd + ", serial: " + serial);
                UssdManager.dialUssd(context, ussd, simSlot);
            }
        } catch (Exception e) {
            e.printStackTrace();
            //requestNewFlexi(context);
            serial++;
            decideNextStep(context);
        }
    }
}
