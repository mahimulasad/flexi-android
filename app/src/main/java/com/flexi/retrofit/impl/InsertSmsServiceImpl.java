package com.flexi.retrofit.impl;

import android.content.Context;

import com.flexi.manager.RetrofitManager;
import com.flexi.manager.UssdManager;
import com.flexi.retrofit.RetrofitService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * @author Mahim
 * @since 9/9/19 5:50 PM
 */
public class InsertSmsServiceImpl {

    private Context context;
    private String sender;
    private String message;

    public void requestInsertSms(Context context, String sender, String message) {
        this.context = context;
        this.sender = sender;
        this.message = message;

        Retrofit retrofit = RetrofitManager.newInstance(context);
        RetrofitService service = retrofit.create(RetrofitService.class);
        Call<String> call = service.insertSms(sender, message);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    handleInsertSmsResponse(response.body());
                } else {
                    //requestInsertSms(context, sender, message);
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //requestInsertSms(context, sender, message);
            }
        });
    }

    private void handleInsertSmsResponse(String response) {
        UssdManager.messageSent(context);
    }
}
