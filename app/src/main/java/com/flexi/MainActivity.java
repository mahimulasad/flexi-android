package com.flexi;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.flexi.constant.BundleKeys;
import com.flexi.manager.PermissionManager;
import com.flexi.manager.PermissionManagerListener;
import com.flexi.manager.SharedPreferenceManager;
import com.flexi.manager.UssdManager;
import com.flexi.service.MainService;

public class MainActivity extends AppCompatActivity
        implements PermissionManagerListener {

    private static String TAG = MainActivity.class.getName();

    private PermissionManager permissionManager;

    private Button startButton;
    private Button stopButton;
    private EditText domainNameEditText;
    private EditText sim1PinEditText;
    private EditText sim2PinEditText;
    private Button saveButton;
    private TextView statusTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init() {
        initPermission();

        initViewReference();
        initViewListener();

        populateUi();
    }

    private void initPermission() {
        permissionManager = new PermissionManager(this);
        permissionManager.checkPermissionAndStartApp(this);
    }

    private void initViewReference() {
        startButton = findViewById(R.id.btn_start);
        stopButton = findViewById(R.id.btn_stop);
        domainNameEditText = findViewById(R.id.et_domain);
        sim1PinEditText = findViewById(R.id.et_gp);
        sim2PinEditText = findViewById(R.id.et_airtel);
        saveButton = findViewById(R.id.btn_save);
        statusTextView = findViewById(R.id.tv_status);
    }

    private void initViewListener() {
        startButton.setOnClickListener((v) -> handleStartButton());
        stopButton.setOnClickListener((v) -> handleStopButton());
        saveButton.setOnClickListener((v) -> handleSaveButton());
    }

    private void populateUi() {
        String domainName = SharedPreferenceManager.getDomainName(this);
        String sim1Pin = SharedPreferenceManager.getSim1Pin(this);
        String sim2Pin = SharedPreferenceManager.getSim2Pin(this);

        if (TextUtils.isEmpty(domainName)) {
            domainName = "";
        }

        if (TextUtils.isEmpty(sim1Pin)) {
            sim1Pin = "";
        }

        if (TextUtils.isEmpty(sim2Pin)) {
            sim2Pin = "";
        }

        domainNameEditText.setText(domainName);
        sim1PinEditText.setText(sim1Pin);
        sim2PinEditText.setText(sim2Pin);
    }

    private void handleStartButton() {
        if (TextUtils.isEmpty(SharedPreferenceManager.getDomainName(this))) {
            Toast.makeText(this, "Domain name is empty, Please Save", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(SharedPreferenceManager.getSim1Pin(this))) {
            Toast.makeText(this, "Sim 1 Pin is empty, Please Save", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(SharedPreferenceManager.getSim2Pin(this))) {
            Toast.makeText(this, "Sim 2 is empty, Please Save", Toast.LENGTH_LONG).show();
            return;
        }

        if (isAccessibilityEnabled()) {
            startService();
        } else {
            new AlertDialog.Builder(this)
                    .setTitle("Permission Request")
                    .setMessage("Give Accessibility Permission by pressing OK button and then press Start again\n\nNote: After pressing OK, from the list of Accessibility service select this app and toggle settings to ON")
                    .setPositiveButton("OK",
                            (dialog, which) -> {
                                Intent settingsIntent = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
                                settingsIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                                startActivity(settingsIntent);
                            })
                    .setCancelable(false)
                    .create().show();
        }
    }

    private void handleStopButton() {
        stopService();
    }

    private void handleSaveButton() {
        String domainName = domainNameEditText.getText().toString();
        String sim1Pin = sim1PinEditText.getText().toString();
        String sim2Pin = sim2PinEditText.getText().toString();

        if (TextUtils.isEmpty(domainName)) {
            Toast.makeText(this, "Domain name is empty", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(sim1Pin)) {
            Toast.makeText(this, "Sim 1 pin is empty", Toast.LENGTH_LONG).show();
            return;
        }
        if (TextUtils.isEmpty(sim2Pin)) {
            Toast.makeText(this, "Sim 2 pin is empty", Toast.LENGTH_LONG).show();
            return;
        }

        SharedPreferenceManager.setDomainName(this, domainName);
        SharedPreferenceManager.setSim1Pin(this, sim1Pin);
        SharedPreferenceManager.setSim2Pin(this, sim2Pin);
        Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
    }

    private void startService() {
        statusTextView.setText("Server status: Connected");
        UssdManager.serverRunning = true;
        Intent mainService = new Intent(this, MainService.class);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            ContextCompat.startForegroundService(this, mainService);
        } else {
            startService(mainService);
        }
    }

    private void stopService() {
        statusTextView.setText("Server status: Disconnected");
        UssdManager.serverRunning = false;
        Intent mainService = new Intent(this, MainService.class);
        mainService.setAction(BundleKeys.Commands.STOP_SERVICE);
        startService(mainService);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        permissionManager.onRequestPermissionsResult(this, requestCode, permissions, grantResults);
    }

    @Override
    public void startApplication() {

    }

    @Override
    public void exitApplication() {
        this.finish();
        android.os.Process.killProcess(android.os.Process.myPid());
    }

    public boolean isAccessibilityEnabled() {
        int accessibilityEnabled = 0;
        final String ACCESSIBILITY_SERVICE_NAME = "com.flexi/com.flexi.service.UssdAccessibilityService";
        boolean accessibilityFound = false;
        try {
            accessibilityEnabled = Settings.Secure.getInt(this.getContentResolver(), android.provider.Settings.Secure.ACCESSIBILITY_ENABLED);
            Log.d(TAG, "ACCESSIBILITY: " + accessibilityEnabled);
        } catch (Settings.SettingNotFoundException e) {
            Log.d(TAG, "Error finding setting, default accessibility to not found: " + e.getMessage());
        }

        TextUtils.SimpleStringSplitter mStringColonSplitter = new TextUtils.SimpleStringSplitter(':');

        if (accessibilityEnabled == 1) {
            Log.d(TAG, "***ACCESSIBILIY IS ENABLED***: ");


            String settingValue = Settings.Secure.getString(getContentResolver(), Settings.Secure.ENABLED_ACCESSIBILITY_SERVICES);
            Log.d(TAG, "Setting: " + settingValue);
            if (settingValue != null) {
                TextUtils.SimpleStringSplitter splitter = mStringColonSplitter;
                splitter.setString(settingValue);
                while (splitter.hasNext()) {
                    String accessabilityService = splitter.next();
                    Log.d(TAG, "Setting: " + accessabilityService);
                    if (accessabilityService.equalsIgnoreCase(ACCESSIBILITY_SERVICE_NAME)) {
                        Log.d(TAG, "We've found the correct setting - accessibility is switched on!");
                        return true;
                    }
                }
            }

            Log.d(TAG, "***END***");
        } else {
            Log.d(TAG, "***ACCESSIBILIY IS DISABLED***");
        }
        return accessibilityFound;
    }
}
