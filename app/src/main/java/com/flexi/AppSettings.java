package com.flexi;

public final class AppSettings {

    public static final String DATABASE_NAME = "flexi_db";
    public static final int DATABASE_VERSION = 2;
}
