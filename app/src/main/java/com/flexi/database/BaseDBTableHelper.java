package com.flexi.database;

import android.database.sqlite.SQLiteDatabase;

public class BaseDBTableHelper {

    public void onCreate(SQLiteDatabase db, String createScript) {
        db.execSQL(createScript);
    }

    public void onUpgrade(SQLiteDatabase db, String tableName, String createScript) {
        db.execSQL("DROP TABLE IF EXISTS " + tableName);
        onCreate(db, createScript);
    }
}
