package com.flexi.database.dao;

import android.database.sqlite.SQLiteDatabase;

import com.flexi.database.BaseDBTableHelper;
import com.flexi.database.IDBTableHelper;

public class CustomSmsDBTableHelper extends BaseDBTableHelper implements IDBTableHelper {

    public static final String TABLE_NAME = "custom_sms";

    public static final String ID = "_id";
    public static final String DATE = "date";
    public static final String READ = "read";
    public static final String ADDRESS = "address";
    public static final String BODY = "body";

    @Override
    public void onCreate(SQLiteDatabase db) {
        super.onCreate(db, getCreateScript());
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        super.onUpgrade(db, TABLE_NAME, getCreateScript());
    }

    @Override
    public String getCreateScript() {
        return "CREATE TABLE " + TABLE_NAME + "("
                + ID + " " + ColumnDataType.LONG + " PRIMARY KEY"
                + ", " + DATE + " " + ColumnDataType.LONG
                + ", " + READ + " " + ColumnDataType.INTEGER
                + ", " + ADDRESS + " " + ColumnDataType.TEXT
                + ", " + BODY + " " + ColumnDataType.TEXT
                + ")";
    }

}
