package com.flexi.database.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.flexi.data.CustomSms;
import com.flexi.database.BaseDao;

import java.util.ArrayList;
import java.util.List;

public class CustomSmsDao extends BaseDao {

    public CustomSmsDao(Context context) {
        super(context);
    }

    public void insert(List<CustomSms> customSmsList) {
        if (customSmsList == null || customSmsList.size() == 0) {
            return;
        }

        SQLiteDatabase db = getDatabase();
        try {
            db.beginTransaction();

            for (CustomSms customSms : customSmsList) {
                if (customSms == null) {
                    continue;
                }
                ContentValues cv = generateContentValues(customSms);

                try {
                    db.insert(CustomSmsDBTableHelper.TABLE_NAME, null, cv);
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }

            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void deleteAll() {
        super.deleteAll(CustomSmsDBTableHelper.TABLE_NAME);
    }

    public void deleteOldSms() {
        int affectedRow = 0;

        SQLiteDatabase db = getDatabase();

        try {
            db.beginTransaction();

            String selection = "date < ?";
            String[] selectionArgs = new String[]{Long.toString(System.currentTimeMillis() - 4L * 3600 * 1000)};

            affectedRow = db.delete(CustomSmsDBTableHelper.TABLE_NAME,
                    selection, selectionArgs);

            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void markAllAsRead() {
        int affectedRow = 0;

        SQLiteDatabase db = getDatabase();

        try {
            db.beginTransaction();

            String selection = "read = ?";
            String[] selectionArgs = new String[]{"0"};

            ContentValues cv = new ContentValues();
            cv.put("read", 1);

            affectedRow = db.update(CustomSmsDBTableHelper.TABLE_NAME, cv,
                    selection, selectionArgs);

            db.setTransactionSuccessful();
            db.endTransaction();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                db.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public List<CustomSms> getUnreadSmsList() {
        List<CustomSms> distributorList = new ArrayList<>();

        SQLiteDatabase db = getDatabase();
        String selection = "read = ?";
        String[] selectionArgs = new String[]{"0"};
        cursor = db.query(CustomSmsDBTableHelper.TABLE_NAME,
                null, selection, selectionArgs, null, null, null);

        try {
            while (cursor.moveToNext()) {
                CustomSms customSms = generateObject();
                distributorList.add(customSms);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }

        return distributorList;
    }

    private ContentValues generateContentValues(CustomSms customSms) {
        if (customSms == null) {
            return null;
        }
        ContentValues cv = new ContentValues();
        cv.put(CustomSmsDBTableHelper.ID, customSms.getId());
        cv.put(CustomSmsDBTableHelper.DATE, customSms.getDate());
        cv.put(CustomSmsDBTableHelper.READ, customSms.getRead());
        cv.put(CustomSmsDBTableHelper.ADDRESS, customSms.getAddress());
        cv.put(CustomSmsDBTableHelper.BODY, customSms.getBody());
        return cv;
    }

    private CustomSms generateObject() {
        CustomSms customSms = new CustomSms();
        if (cursor != null) {
            customSms.setId(getLong(CustomSmsDBTableHelper.ID));
            customSms.setDate(getLong(CustomSmsDBTableHelper.DATE));
            customSms.setRead(getInt(CustomSmsDBTableHelper.READ));
            customSms.setAddress(getString(CustomSmsDBTableHelper.ADDRESS));
            customSms.setBody(getString(CustomSmsDBTableHelper.BODY));
        }
        return customSms;
    }
}
