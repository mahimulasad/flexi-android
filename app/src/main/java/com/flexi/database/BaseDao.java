package com.flexi.database;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

public class BaseDao {

    private Context context;

    protected Cursor cursor;

    protected BaseDao(Context context) {
        this.context = context;
    }

    protected SQLiteDatabase getDatabase() {
        return SQLiteOpenHelper.getInstance(context).getWritableDatabase();
    }

    protected Integer getInt(String columnName) {
        return cursor.getInt(cursor.getColumnIndex(columnName));
    }

    protected Long getLong(String columnName) {
        int columnIndex = cursor.getColumnIndex(columnName); //TRY: getColumnIndexOrThrow
        if (!cursor.isNull(columnIndex)) {
            return cursor.getLong(columnIndex);
        }
        return null;
    }

    @Deprecated
    protected boolean getBoolean(Cursor cursor, String columnName) {
        boolean ret = false;
        if (cursor.getInt(cursor.getColumnIndex(columnName)) == 1) {
            ret = true;
        }
        return ret;
    }

    protected boolean getBoolean(String columnName) {
        boolean ret = false;
        if (cursor.getInt(cursor.getColumnIndex(columnName)) == 1) {
            ret = true;
        }
        return ret;
    }

    protected String getString(String columnName) {
        return cursor.getString(cursor.getColumnIndex(columnName));
    }

    protected Double getDouble(String columnName) {
        return cursor.getDouble(cursor.getColumnIndex(columnName));
    }

    protected String[] getArray(String columnName) {
        String listString = getString(columnName);
        return listString.split(",");
    }

    /***
     * @Deprecated, use getListOfLong() instead
     */
    @Deprecated
    protected Long[] getArrayOfLong(String columnName) {
        String listString = getString(columnName);
        String[] stringArray = listString.split(",");
        Long[] longArray = new Long[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            longArray[i] = Long.valueOf(stringArray[i]);
        }
        return longArray;
    }

    protected void deleteAll(String tableName) {
        SQLiteDatabase db = getDatabase();
        try {
            db.execSQL("DELETE FROM " + tableName);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    protected void delete(String tableName, Long id) {
        if (id == null) {
            return;
        }
        SQLiteDatabase db = getDatabase();
        try {
            db.delete(tableName, "id=?", new String[]{"" + id});
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            db.close();
        }
    }

    protected String getStringValue(Boolean b) {
        return b == null ? "" : (b ? "1" : "0");
    }

    protected String getStringValue(Integer integer) {
        String ret = "";
        if (integer == null) {
            return ret;
        }
        try {
            ret = String.valueOf(integer);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return ret;
    }

    protected String getStringValue(Long aLong) {
        String ret = "";
        if (aLong == null) {
            return ret;
        }
        try {
            ret = String.valueOf(aLong);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return ret;
    }
}
