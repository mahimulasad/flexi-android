package com.flexi.database;

import android.database.sqlite.SQLiteDatabase;

public interface IDBTableHelper {

    class ColumnDataType {
        public static final String TEXT = "TEXT";
        public static final String INTEGER = "INTEGER";
        public static final String LONG = "INTEGER";
        public static final String BOOLEAN = "INTEGER";
        public static final String DOUBLE = "DOUBLE";
    }

    public void onCreate(SQLiteDatabase db);

    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion);

    public String getCreateScript();
}
