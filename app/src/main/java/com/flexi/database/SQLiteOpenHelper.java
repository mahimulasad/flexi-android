package com.flexi.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

import com.flexi.AppSettings;
import com.flexi.database.dao.CustomSmsDBTableHelper;

public class SQLiteOpenHelper extends android.database.sqlite.SQLiteOpenHelper {

    private static SQLiteOpenHelper instance;

    private IDBTableHelper[] tables = new IDBTableHelper[]{
            new CustomSmsDBTableHelper()
    };

    private SQLiteOpenHelper(Context context) {
        super(context, AppSettings.DATABASE_NAME, null, AppSettings.DATABASE_VERSION);
    }

    public static SQLiteOpenHelper getInstance(Context context) {
        if (instance == null) {
            instance = new SQLiteOpenHelper(context);
        }
        return instance;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        for (IDBTableHelper table : tables) {
            table.onCreate(db);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        for (IDBTableHelper table : tables) {
            table.onUpgrade(db, oldVersion, newVersion);
        }
    }
}
