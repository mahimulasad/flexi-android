package com.flexi.manager;

public interface PermissionManagerListener {
    void startApplication();
    void exitApplication();
}
