package com.flexi.manager;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionManager {

    private PermissionManagerListener permissionManagerListener;

    private static String[] permissions = new String[]{
            Manifest.permission.CALL_PHONE,
            Manifest.permission.READ_SMS
            //Manifest.permission.BIND_ACCESSIBILITY_SERVICE
    };

    private static final int PERMISSION_REQUEST_ALL = 101;

    private static final String TAG = PermissionManager.class.getClass().getName();

    public PermissionManager(PermissionManagerListener permissionManagerListener) {
        this.permissionManagerListener = permissionManagerListener;
    }

    public void checkPermissionAndStartApp(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkAndAskPermissionThenProceed((Activity) context, permissions, PERMISSION_REQUEST_ALL);
        } else {
            permissionManagerListener.startApplication();
        }
    }

    private void checkAndAskPermissionThenProceed(Activity activity, String[] permissions, int permissionId) {
        int granted = 0;
        for (String permission : permissions) {
            if (ContextCompat.checkSelfPermission(activity, permission) == PackageManager.PERMISSION_GRANTED) {
                granted++;
            }
        }

        if (granted == permissions.length) {
            permissionManagerListener.startApplication();
        } else if (granted < permissions.length) {
            try {
                ActivityCompat.requestPermissions(activity, permissions, permissionId);
            } catch (Exception e) {
                Log.e(TAG, "Permission Exception: " + e.toString());
            }
        }
    }

    public void onRequestPermissionsResult(Context context, int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_ALL:
                int granted = 0;
                boolean permissionAlreadyDenied = false;
                for (int i = 0; i < grantResults.length; i++) {
                    if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                        granted++;
                    } else if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context, permissions[i])) {
                        permissionAlreadyDenied = true;
                    }
                }

                if (permissionAlreadyDenied) {
                    showAlertAndExit(context, "All permissions are needed to proceed.\\nYou have permanently denied one or more permission.\\nPlease Clear data/Uninstall the application and Start the application again.");
                } else if (granted < grantResults.length) {
                    showAlertAndExit(context, "All permissions are needed to proceed.\\nPlease Restart the application and allow all permissions.");
                } else {
                    permissionManagerListener.startApplication();
                }
        }
    }

    private void showAlertAndExit(Context context, String message) {
        new AlertDialog.Builder(context)
                .setTitle("Error")
                .setMessage(message)
                .setPositiveButton("OK",
                        (dialog, which) -> permissionManagerListener.exitApplication())
                .create().show();
    }
}
