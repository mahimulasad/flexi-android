package com.flexi.manager;

import android.content.Context;

import com.flexi.util.SharedPreferenceUtil;

public class SharedPreferenceManager {

    private static final String PREF_DOMAIN_NAME = "PREF_DOMAIN_NAME";
    private static final String PREF_SIM1_PIN = "PREF_SIM1_PIN";
    private static final String PREF_SIM2_PIN = "PREF_SIM2_PIN";

    public static String getDomainName(Context context) {
        return SharedPreferenceUtil.getString(context, PREF_DOMAIN_NAME, null);
    }

    public static void setDomainName(Context context, String value) {
        SharedPreferenceUtil.setString(context, PREF_DOMAIN_NAME, value);
    }

    public static String getSim1Pin(Context context) {
        return SharedPreferenceUtil.getString(context, PREF_SIM1_PIN, null);
    }

    public static void setSim1Pin(Context context, String value) {
        SharedPreferenceUtil.setString(context, PREF_SIM1_PIN, value);
    }

    public static String getSim2Pin(Context context) {
        return SharedPreferenceUtil.getString(context, PREF_SIM2_PIN, null);
    }

    public static void setSim2Pin(Context context, String value) {
        SharedPreferenceUtil.setString(context, PREF_SIM2_PIN, value);
    }
}
