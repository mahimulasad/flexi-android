package com.flexi.manager;

import android.content.Context;
import android.util.Log;

import com.flexi.exception.RetrofitNoConnectivityException;
import com.flexi.util.NetworkUtil;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class RetrofitManager {

    public static final String TAG = "FlexiApi";
    private static boolean logEnabled = true;

    public static Retrofit newInstance(final Context context) {

//        Gson gson = new GsonBuilder()
//                .setFieldNamingPolicy(FieldNamingPolicy.UPPER_CAMEL_CASE)
//                .create();

        String url = "http://" + SharedPreferenceManager.getDomainName(context) + "/";

        Retrofit.Builder retrofitBuilder = new Retrofit.Builder()
                .baseUrl(url)
                .addConverterFactory(ScalarsConverterFactory.create());

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.connectTimeout(5 * 60, TimeUnit.SECONDS);
        httpClient.readTimeout(5 * 60, TimeUnit.SECONDS);
        httpClient.writeTimeout(5 * 60, TimeUnit.SECONDS);
        httpClient.addInterceptor(new ConnectivityInterceptor(Observable.just(NetworkUtil.isNetworkConnected(context))));

        if (logEnabled) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor(
                    new HttpLoggingInterceptor.Logger() {
                        @Override
                        public void log(String message) {
                            Log.d(TAG, message);
                        }
                    }
            );
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

            httpClient.addInterceptor(loggingInterceptor);
        }

        retrofitBuilder.client(httpClient.build());

        return retrofitBuilder.build();
    }

    static class ConnectivityInterceptor implements Interceptor {

        private boolean isNetworkActive;

        public ConnectivityInterceptor(Observable<Boolean> isNetworkActive) {
            isNetworkActive.subscribe(
                    _isNetworkActive -> this.isNetworkActive = _isNetworkActive,
                    _error -> Log.e(TAG, "NetworkActive error " + _error.getMessage())
            );
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
            if (!isNetworkActive) {
                throw new RetrofitNoConnectivityException();
            } else {
                Response response = chain.proceed(chain.request());
                return response;
            }
        }
    }
}
