package com.flexi.manager;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.flexi.data.CustomSms;
import com.flexi.database.dao.CustomSmsDao;
import com.flexi.retrofit.impl.InsertSmsServiceImpl;
import com.flexi.retrofit.impl.NewFelxiServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class UssdManager {

    public static int serial = 1;
    public static int messageCount = 0;
    public static boolean serverRunning = false;

    //sim slot 0 and 1
    public static void dialUssd(Context context, String ussdCode, int simSlotNumber) {
        Log.d("UssdManager", "ussd: " + ussdCode + ", sim: "+ simSlotNumber);
        Intent intent = new Intent(Intent.ACTION_CALL);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        for (String simSlot : simSlotNames) {
            intent.putExtra(simSlot, simSlotNumber);
        }
        intent.setData(ussdToCallableUri(ussdCode));
        try {
            if ((ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE)
                    == PackageManager.PERMISSION_GRANTED)) {
                context.startActivity(intent);
            } else {
                new AlertDialog.Builder(context)
                        .setTitle("Error")
                        .setMessage("Call Phone Permission not given")
                        .setPositiveButton("OK", null)
                        .setCancelable(false)
                        .create().show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static Uri ussdToCallableUri(String ussd) {
        String uriString = "";
        if (!ussd.startsWith("tel:")) {
            uriString += "tel:";
        }
        for (char c : ussd.toCharArray()) {
            if (c == '#') {
                uriString += Uri.encode("#");
            } else {
                uriString += c;
            }
        }
        return Uri.parse(uriString);
    }

    private final static String simSlotNames[] = {
            "extra_asus_dial_use_dualsim",
            "com.android.phone.extra.slot",
            "slot",
            "simslot",
            "sim_slot",
            "subscription",
            "Subscription",
            "phone",
            "com.android.phone.DialingMode",
            "simSlot",
            "slot_id",
            "simId",
            "simnum",
            "phone_type",
            "slotId",
            "slotIdx"
    };

    public static void decideNextStep(Context context) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!serverRunning) return;
                int mod = serial % 3;
                if (mod == 1 || mod == 2) {
                    new NewFelxiServiceImpl().requestNewFlexi(context);
                } else {
                    sendAllUnreadSms(context);
                }
            }
        }, 10 * 1000);
    }

    public static void messageSent(Context context) {
        messageCount--;
        if (messageCount < 0) {
            messageCount = 0;
        }
        if (messageCount == 0) {
            decideNextStep(context);
        }
    }

    public static void sendAllUnreadSms(Context context) {
        List<CustomSms> smsList = new ArrayList<>();
        Uri uri = null;
        String selection = null;
        String[] selectionArgs = new String[0];
        try {
            uri = Uri.parse("content://sms/inbox");
            selection = "read = ? and date >= ?";
            selectionArgs = new String[]{"0", Long.toString(System.currentTimeMillis() - 2L * 3600 * 1000)};
            Log.d("UssdManager", "" + selectionArgs[1]);
            Cursor cur = context.getContentResolver().query(uri,
                    null, selection, selectionArgs, null);
            while (cur != null && cur.moveToNext()) {
                try {
                    long id = cur.getLong(cur.getColumnIndexOrThrow("_id"));
                    long date = cur.getLong(cur.getColumnIndexOrThrow("date"));
                    int read = cur.getInt(cur.getColumnIndexOrThrow("read"));
                    String address = cur.getString(cur.getColumnIndex("address"));
                    String body = cur.getString(cur.getColumnIndexOrThrow("body"));
                    Log.d("UssdManager", "name: " + address + ", _id: " + id + ", date: " + date + ", body: " + body);

                    CustomSms customSms = new CustomSms(id, date, read, address, body);
                    smsList.add(customSms);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            if (cur != null) {
                cur.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            CustomSmsDao dao = new CustomSmsDao(context);
            dao.insert(smsList);

            List<CustomSms> unreadSmsList = dao.getUnreadSmsList();
            dao.markAllAsRead();
            dao.deleteOldSms();

            serial++;

            if (unreadSmsList == null || unreadSmsList.size() == 0) {
                decideNextStep(context);
            } else {
                messageCount += unreadSmsList.size();
                for (CustomSms customSms : unreadSmsList) {
                    if (customSms != null) {
                        new InsertSmsServiceImpl().requestInsertSms(context,
                                customSms.getAddress(), customSms.getBody());
                    } else {
                        messageCount--;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
